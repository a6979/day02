﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2
{
	class Program
	{
		static void Main(string[] args)
		{
			var depth = 0;
			var horizontal = 0;
			var aim = 0;
			var strings = File.ReadAllLines("input.txt").ToList();
			List<string[]> arrayList = new List<string[]>();
			foreach (var text in strings)
			{
				arrayList.Add(text.Split(" "));
			}

			foreach (var move in arrayList)
			{
				var direction = move[0];
				var value = Int32.Parse(move[1]);
				if (direction.Equals("forward"))
				{
					horizontal += value;
				}
				if (direction.Equals("up"))
				{
					depth -= value;
				}
				if (direction.Equals("down"))
				{
					depth += value;
				}
			}

			foreach (var move in arrayList)
			{
				var direction = move[0];
				var value = Int32.Parse(move[1]);
				if (direction.Equals("forward"))
				{
					horizontal += value;
					if (aim != 0)
					{ 
						depth += aim * value;
					}
				}
				if (direction.Equals("down"))
				{
					aim += value;
				}
				if (direction.Equals("up")) 
				{
					aim -= value;
				}
			}

			Console.WriteLine("aim = " + aim);
			Console.WriteLine("horizontal = " + horizontal);
			Console.WriteLine(depth * horizontal);
		}
	}
}
